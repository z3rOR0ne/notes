# Brian's Big List of Various Media

## Podcasts

### Tech Podcasts

- [Syntax]()
- [Software Engineering Daily]()
- [Backend Banter]()
- [Python Bytes]()
- [Shop Talk]()
- [Rustacean Station]()
- [The Free Code Camp Podcast]()
- [Dot Social]()
- [Rust In Production]()
- [Software Unscripted]()
- [Developer Voices]()
- [Book Overflow]()
- [Darknet Diaries]()

### Political Podcasts

- [The Majority Report]()
- [Behind The Bastards]()
- [It Could Happen Here]()
- [I Hate Bill Maher]()
- [Some More News]()
- [Party Girls]()
- [Rev Left Radio]()
- [The Dugout]()
- [Cool People Who Did Cool Stuff]()
- [Knowledge Fight]()
- [Final Straw]()
- [Climate Conversations]()
- [The Bitchuation Room]()
- [The Antifada]()
- [Factually with Adam Conover]()
- [Hood Politics]()
- [Pod Damn America]()
- [IT'S GOING DOWN]()

### Tech/Political Podcasts

- [Citation Needed]()
- [This Machine Kills]()
- [System Crash]()
- [Better Offline]()
- [Tech Won't Save Us]()
- [The 404 Media Podcast]()
- [There Are No Girls On The Internet]()

### Philosophy Podcasts

- [Philosophize This!]()

### Medical Podcasts

- [The Checkup with Doctor Mike]()
- [The House of Pod]()

### Psychadelic Chemistry

- [The Hamilton Morris Podcast]()

## Youtube Channels

### Programming

#### General

- [Theo -t3.gg]()
- [ThePrimeTime]()
- [ThePrimeagen]()
- [Anthony GG]()
- [Bro Code]()
- [Computerphile]()
- [Dave's Garage]()
- [DThompsonDev]()
- [freeCodeCamp.org]()
- [Fun Fun Function]()
- [Melkey]()
- [MIT OpenCourseWare]()
- [Software Unscripted]()
- [Spiro Floropoulos]()
- [The Coding Train]()
- [Traversy Media]()

#### OS/Networking/DB Internals

- [Hussein Nasser]()
- [Arpit Bhayani]()

#### System Design

- [Caleb Curry]()
- [Casey Muratori]()
- [Molly Rocket]()

#### Web Dev

- [Net Ninja]()
- [Programming with Mosh]()
- [Coder Coder]()
- [Coding Garden]()
- [dcode]()
- [developedbyed]()
- [Frontend Masters]()
- [Jack Herrington]()
- [James Q Quick]()
- [Kent C. Dodds]()
- [Kevin Powell]()
- [Matt Pocock]()
- [Program With Erik]()
- [Ryan Carniato]()
- [Syntax]()
- [Web Dev Cody]()
- [Web Dev Simplified]()
- [Wes Bos]()
- [WittCode]()

#### Python

- [ArjanCodes]()

#### Rust

- [fasterthanlime]()
- [Francesco Ciulla]()
- [Let's Get Rusty]()

#### Low Level

- [TSoding Daily]()
- [Low Level]()
- [Low Level Game Dev]()
- [marcan]()

#### Hacking/Security

- [NetworkChuck]()
- [David Bombal]()
- [John Hammond]()
- [Hak5]()
- [Sumsub]()

#### Privacy

- [The Hated One]()

#### Video Game Creation

- [DaFluffyPotato]()

#### Linux

- [Brodie Robertson]()
- [Nicco Loves Linux]()
- [Trafotin]()
- [BusWriter]()
- [DJ Ware]()
- [Learn Linux TV]()
- [Michael Horn]()
- [Red Hat Enterprise Linux]()
- [The Linux Experiment]()
- [unfa]()
- [Veronica Explains]()

#### Vim/Bash/Scripting

- [ThePrimeTime]()
- [ThePrimeagen]()
- [bashbunni]()
- [teej_daily]()
- [TJ DeVries]()

#### DevOps

- [DevOps Toolbox]()
- [DevOps Toolkit]()
- [TechWorld with Nana]()

#### Hardware

- [JayzTwoCents]()
- [Linus Tech Tips]()
- [Gary Explains]()
- [Jeff Geerling]()
- [Talking Sasquach]()

#### Data Structures/Algorithms/Leetcode

- [NeetCodeIO]()
- [Deepti Talesra]()
- [Abdul Bari]()
- [Michael Sambol]()

### Political

- [Adam Conover]()
- [Democracy Now!]()
- [Diego Ruzzarin]()
- [Double Down News]()
- [Joshua Fluke]()
- [LastWeekTonight]()
- [LegalEagle]()
- [Logically Answered]()
- [Molly White]()
- [Rebecca Watson (Skepchick)]()
- [Some More News]()
- [The Bitchuation Room (with Francesca Fiorentini)]()
- [The Kavernacle]()
- [The Leftist Cooks]()
- [Tom Nicholas]()
- [World Beyond Capitalism]()

### Philsophy/Spirituality

- [Alan Watts]()
- [Ram Dass]()
- [Be Here Now Network]()
- [MagickMe]()

### Anarchism

- [Anark]()
- [Andrewism]()
- [Zoe Baker]()

### Trans Rights

- [Aranock]()
- [Bennie Carollo]()
- [ContraPoints]()
- [Council of Geeks]()
- [Jessie Gender]()
- [Lily Simpson]()
- [MATRIARCHETYPE]()
- [Philosophy Tube]()
- [Sophie from Mars]()

### Music

- [Adeem The Artist]()
- [Asian Kung-Fu Generation]()
- [MrSuicideSheep]()
- [Proximity]()
- [Roots and Recognition - Topic]()
- [Silva Hound]()
- [ThePrimeThanatos]()
- [White Bat Audio]()

### Keyboards, Life Hacks

- [Ben Vallack]()

### Comedy/Entertainment

- [Cracked]()
- [Dorkly]()
- [Dr. Jordan Breeding]()
- [Eddy Burback]()
- [Georgia Dow]()
- [Honest Ads]()
- [Husky]()
- [Ice Cream Sandwich]()
- [Jared Bower]()
- [Jessie Gender After Dark]()
- [Joma Tech]()
- [Julie Nolke]()
- [Like Stories of Old]()
- [Lindsay Ellis]()
- [Michael Swaim]()
- [NakeyJakey]()
- [Noodle]()
- [OrangeRiver]()
- [Pitch Meeting]()
- [ProfessorViral]()
- [Ryan Hollinger]()
- [Salar]()
- [Small Beans]()
- [TheBurgerkrieg]()
- [Tom Scott]()
- [Viva La Dirt League]()
- [WiseCrack]()

### Medical

- [Doctor Mike]()

### Economics

- [Economics Explained]()
- [How Money Works]()

### Cats

- [Jackson Galaxy]()

### Animation

- [Lackadaisy]()
- [Vivziepop]()
- [Winston Rowntree]()

### Animation Reviews

- [Johnny 2 Cellos]()
- [Kitty Monk]()
- [offbeat kiki]()
- [ToonrificTariq]()

### Anime Reviews

- [Gigguk]()

### Mathematics

- [Numberphile]()
- [Professor Leonard]()
- [The Math Sorceror]()

### Fantasy/Sci Fi Book Reviews

- [The Book Guy]()
